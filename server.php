<?php


session_start();

class LoginForm {


	public function login($username, $password) {
		$newUser = (object) [

			'username' => $username,
			'password' => $password
		];

		if($_SESSION['login'] === null) {
			$_SESSION['login'] = array();
		}

		array_push($_SESSION['login'], $newUser);
	}

	public function clear() {
		session_destroy();
	}


};







$loginForm = new LoginForm();


if($_POST['action'] === 'login') {
	$loginForm->login($_POST['username']);
} else if ($_POST['action'] === 'clear') {
	$loginForm->clear();
}