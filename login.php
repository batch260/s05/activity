<?php
session_start();

if (isset($_POST['username'])) {
  $_SESSION['username'] = $_POST['username'];
 
}
?>

<!DOCTYPE html>  
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Logged in page</title>
</head>
<body>
	<div>
		<?php if (isset($_SESSION['username'])): ?>
			<p>Hello, <?php echo $_SESSION['username']; ?></p>
			<form method="POST" action="./" value="clear">
	            <button type="submit">Logout</button>
	        </form>
		
		<?php endif; ?>
	</div>
</body>
</html>